-- Loads all required fonts

function loadF(file, size)
    return g.newFont("fonts/"..file, size)
end

font = {
    big = loadF("Xolonium-Regular.otf",32),
    medium = loadF("Xolonium-Regular.otf",24),
    small = loadF("Xolonium-Regular.otf",16),
    hutsut = loadF("HutSutRalstonNF.ttf", 96),
}
