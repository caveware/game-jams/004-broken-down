-- Load all useable objects

require "code/startup/objects/player"        -- Load the player

block = { }
blocks = { }

block[1] = {
    x = 0,
    y = 0,
    width = 32,
    height = 256,
}
block[2] = {
    x = 256,
    y = 0,
    width = 32,
    height = 256,
}
block[3] = {
    x = 0,
    y = 256,
    width = 288,
    height = 32,
}

function blocks.draw()
    g.setColor(200, 150, 125)
    for k,v in ipairs(block) do
        g.rectangle("fill", v.x, v.y, v.width, v.height)
    end
end
