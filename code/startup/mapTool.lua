-- Map loading system

map = { }
internal = { }
levelName = { }
death = { }
arches = { }
portal = { }
warps = { }
pWarps = { }
local mapData, tilesB, tilesD, images, rectsB, rectsD, portals, switches

function map.load(file, tot)
    player.setup()
    tilesB  = { }
    tilesD  = { }
    images  = { }
    rectsB  = { }
    rectsD  = { }
    quads   = { }
    portals = { }
    switches= { }
    death   = { }
    arches  = { }
    portal  = { }
    warps   = { }
    pWarps  = { }

    levelName = file
    mapData = require(file)

    for k,v in ipairs(mapData.tilesets) do
        local i, marg, vspac, tmp
        images[k] = getResourceFromString(v.image)
        -- Load quads from tileset
        i = v.firstgid
        vspac = v.margin
        for h=0, math.ceil((v.imageheight - v.spacing) / v.tileheight) - 1 do
            marg = v.margin
            for w=0, math.ceil((v.imagewidth - v.spacing * v.tilewidth) / v.tilewidth) - 1 do
                quads[i] = g.newQuad(marg + (w * v.tilewidth), vspac + (h * v.tileheight), v.tilewidth, v.tileheight, v.imagewidth, v.imageheight)
                marg = marg + v.spacing
                i = i + 1
            end
            vspac = vspac + v.spacing
        end
    end

    for k,v in ipairs(mapData.layers) do
        if v.name == "babyCollision" then
            for k,v in ipairs(v.objects) do
                rectsB[#rectsB+1] = {
                    x = v.x,
                    y = v.y,
                    width = v.width,
                    height = v.height,
                    solid = true
                }
                if v.gid ~= nil then
                    mm = rectsB[#rectsB]
                    mm.name = v.name
                    mm.gid = v.gid
                    useless1, useless2, mm.width, mm.height = quads[v.gid]:getViewport()
                    mm.y = mm.y - mm.height
                    if v.properties["hide"] ~= nil then
                        mm.solid = false
                    end
                end
            end
        elseif v.name == "dadCollision" then
            for k,v in ipairs(v.objects) do
                rectsD[#rectsD+1] = {
                    x = v.x,
                    y = v.y,
                    width = v.width,
                    height = v.height,
                    solid = true
                }
                if v.gid ~= nil then
                    mm = rectsD[#rectsD]
                    mm.name = v.name
                    mm.gid = v.gid
                    useless1, useless2, mm.width, mm.height = quads[v.gid]:getViewport()
                    mm.y = mm.y - mm.height
                    if v.properties["hide"] ~= nil then
                        mm.solid = false
                    end
                end
            end
        elseif v.name == "switches" then
            for k,v in ipairs(v.objects) do
                switches[#switches+1] = {
                    x = v.x,
                    y = v.y - 32,
                    width = 32,
                    height = 32,
                    on = true,
                    name = v.name,
                    add = v.properties["addBlock"],
                    remove = v.properties["removeBlock"],
                    toggle = v.properties["toggleSwitch"],
                }
                if v.properties["on"] == "true" then
                    sToggle(switches[#switches])
                end
            end
        elseif v.name == "warp" then
            for k,v in ipairs(v.objects) do
                warps[#warps+1] = {
                    x = v.x,
                    y = v.y - 32,
                    width = 32,
                    height = 32,
                    name = v.name,
                    link = v.properties["link"],
                    gid = v.gid
                }
                pWarps[#pWarps+1] = {
                    x = v.x,
                    y = v.y - 64,
                    width = 32,
                    height = 32,
                    name = v.name,
                    link = v.properties["link"],
                }
            end
        elseif v.name == "arch" then
            for k,v in ipairs(v.objects) do
                arches[#arches+1] = {
                    name = v.name,
                    x = v.x,
                    y = v.y - 128,
                    width = 128,
                    height = 128,
                    next = v.properties["nextArch"],
                    exclusive = v.properties["exclusive"],
                    gid = v.gid,
                }
            end
        elseif v.name == "death" then
            for k,v in ipairs(v.objects) do
                death[#death+1] = {
                    x = v.x,
                    y = v.y,
                    width = v.width,
                    height = v.height
                }
            end
        elseif v.name == "important" then
            for k,v in ipairs(v.objects) do
                if v.name == "playerStart" then
                    player.x = v.x - (player.width - v.width) / 2
                    player.y = v.y - player.height + v.height
                    totems.setup(v.x, v.y, v.width, v.height)
                elseif v.name == "portal" and v.gid == nil then
                    portal = {
                        x = v.x,
                        y = v.y,
                        width = v.width,
                        height = v.height,
                        level = v.properties["nextLevel"]
                    }
                elseif v.name == "portal" and v.gid ~= nil then
                    portal = {
                        x = v.x,
                        y = v.y - 128,
                        width = 128,
                        height = 128,
                        gid = v.gid,
                        level = v.properties["nextLevel"]
                    }
                elseif v.gid ~= nil then
                    _G[v.name] = {
                        x = v.x,
                        y = v.y - map.getImage(v.gid):getHeight(),
                        width = map.getImage(v.gid):getWidth(),
                        height = map.getImage(v.gid):getHeight(),
                        image = map.getImage(v.gid)
                    }
                else
                    _G[v.name] = {
                        x = v.x,
                        y = v.y,
                        width = v.width,
                        height = v.height,
                    }
                end
            end
        end
    end
end

function map.draw(vx, vy)
    g.setColor(255, 255, 255)
    local tmp_x, tmp_y
    for k,v in ipairs(mapData.layers) do
        multi = 1
        if v.name == "background" then
            multi = 0.9
            for k,v in ipairs(v.objects) do
                yup = map.getImage(v.gid)
                g.draw(yup, quads[v.gid], v.x - vx * multi, v.y - vy - yup:getHeight())
            end
        end
        can = true
        if gameState == "play" and v.name ~= player.state .. "Layer" then
            can = false
        end
        if v.type == "tilelayer" and can then
            -- differen layers change multi
            for k,v in ipairs(v.data) do
                tmp_y = math.floor((k - 1) / mapData.width)
                tmp_x = k - (tmp_y * mapData.width) - 1
                if v > 0 then
                    checkkk = {
                        x = tmp_x * mapData.tilewidth - 64,
                        y = tmp_y * mapData.tileheight - 64
                    }
                    ttt, ttp, checkkk.width, checkkk.height = quads[v]:getViewport()
                    checkkk.y = checkkk.y - (checkkk.height - 32)
                    checkkk.width = checkkk.width + 128
                    checkkk.height = checkkk.height + 128
                    getScale()
                    vuu = {
                        x = view.x,
                        y = view.y,
                        width = view.width,
                        height = view.height
                    }
                    if math.overlap(checkkk, vuu) then
                    u1, u2, u3, n = quads[v]:getViewport()
                    g.draw(map.getImage(v), quads[v], tmp_x * mapData.tilewidth - vx * multi, tmp_y * mapData.tileheight - vy * multi - (n - 32))
                    end
                end
            end
        end
    end
    if player.state == "baby" then arr = rectsB
    else arr = rectsD end
    for k,v in ipairs(arr) do
        if v.gid ~= nil and v.solid then
            g.draw(map.getImage(v.gid), quads[v.gid], v.x - vx, v.y - vy)
        end
    end
    for k,v in ipairs(switches) do
        vey = v.y - vy
        num = 1 ; if not v.on then num = 2 ; vey = vey - 16 end
        g.draw(gfx.switch[num], v.x - vx, vey)
    end
    for k,v in ipairs(arches) do
        dd = true
        if v.exclusive ~= nil then
            if v.exclusive ~= player.state then
                dd = false
            end
        end
        if dd then g.draw(map.getImage(v.gid), quads[v.gid], v.x - vx, v.y - vy) end
    end
    for k,v in ipairs(warps) do
        g.draw(map.getImage(v.gid), quads[v.gid], v.x - vx, v.y - vy)
    end
    if gameState == "intro" then
        g.draw(car.image, car.x - vx, car.y - vy)
    end
    if portal.gid ~= nil then
        v = portal
        g.draw(map.getImage(v.gid), quads[v.gid], v.x - vx, v.y - vy)
    end
end

function map.getImage(v)
    local num
    num = #mapData.tilesets
    while (v < mapData.tilesets[num].firstgid) do
        num = num - 1
    end
    return images[num]
end

function map.touches(obj)
    returnValue = false
    if player.state == "baby" then arr = rectsB
    else arr = rectsD end
    for k,v in ipairs(arr) do
        if math.overlap(obj, v) and v.solid then
            returnValue = true
        end
    end
    for k,v in ipairs(switches) do
        if math.overlap(obj, v) then
            if v.on and player.ySpeed > 0 then
                sfx.switch:play()
                v.on = false
                v.y = v.y + 32
                v.height = 0
                for k,b in ipairs(rectsB) do
                    if b.name == v.add and v.add ~= nil then
                        b.solid = true
                    elseif b.name == v.remove and v.remove ~= nil then
                        b.solid = false
                    end
                end
                for k,b in ipairs(rectsD) do
                    if b.name == v.add and v.add ~= nil then
                        b.solid = true
                    elseif b.name == v.remove and v.remove ~= nil then
                        b.solid = false
                    end
                end
                for k,b in ipairs(switches) do
                    if b.name == v.toggle and v.toggle ~= nil then
                        sToggle(b)
                    end
                end
            else
                returnValue = true
            end
        end
    end
    return returnValue
end

function map.getWidth()
    return mapData.width * mapData.tilewidth
end

function map.getHeight()
    return mapData.height * mapData.tileheight
end

function sToggle(ss)
    if ss.on then
        ss.on = false
        ss.y = ss.y + 32
        ss.height = 0
    else
        ss.on = true
        ss.y = ss.y - 32
        ss.height = 32
    end
end

-- Convert resource from string to actual data
-- eg. 'gfx.shrek' to gfx.shrek's image data
-- Used by mapTool
function getResourceFromString(resource)

    -- Remove unneccessary parts of filename
    resource = resource:gsub(".jpg", "")
    resource = resource:gsub(".ogg", "")
    resource = resource:gsub(".png", "")
    resource = resource:gsub("%.%./", "")

    -- Replace slashes with periods to simplify loading
    resource = resource:gsub("/", "\\")

    -- Convert current string to table of strings
    tab = { }
    for word in resource:gmatch("([^\\]+)") do
        tab[#tab + 1] = word
    end

    -- Recursively access deeper parts of table containing resource
    for key, value in ipairs(tab) do
        if key == 1 then
            resource = _G[value]
        else
            resource = resource[value]
        end
    end

    -- Return the found resource
    return(resource)

end
