-- Shortcuts
k = love.keyboard
g = love.graphics
w = love.window
fs = love.filesystem

g.setDefaultFilter("nearest", "nearest")

-- Load resources on startup

require "code/startup/extraMath"
require "code/startup/loadBGM"
require "code/startup/loadSFX"
require "code/startup/loadFonts"
require "code/startup/loadGFX"
require "code/startup/loadObjects"
require "code/startup/mapTool"
require "code/view"
require "code/androidMagic"
require "code/drawFunctions"
require "code/totems"
require "code/soundMagic"
