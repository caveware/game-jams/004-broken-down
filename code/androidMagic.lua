function overlay()
    g.setColor(128, 128, 128, 128)
    g.rectangle("fill", 0, 400, 192, 80)
    g.rectangle("fill", 200, 400, 192, 80)
    g.rectangle("fill", 662, 400, 192, 80)
    g.rectangle("fill", 662, 312, 192, 80)
    g.rectangle("fill", 662, 224, 192, 80)
    g.setColor(0, 0, 0, 255)
    g.setFont(font.medium)
    win = g.getFont():getWidth("LEFT")
    wil = g.getFont():getWidth("RIGHT")
    wio = g.getFont():getWidth("JUMP")
    wiq = g.getFont():getWidth("SWAP")
    wiu = g.getFont():getWidth("UP")
    hie = g.getFont():getHeight()
    g.print("LEFT", 96 - win/ 2, 440 - hie / 4, 0)
    g.print("RIGHT", 288 - wil/ 2, 440 - hie / 4, 0)
    g.print("JUMP", 758 - wil/ 2, 440 - hie / 4, 0)
    g.print("SWAP", 758 - wil/ 2, 352 - hie / 4, 0)
    g.print("UP", 758 - wil/ 2, 264 - hie / 4, 0)
end

function love.touchpressed(gid, gx, gy, gp)
    if nextState == "instr" then
        gameState = "menu"
        nextState = "menu"
    end
    gx = gx * 854
    gy = gy * 480
    scrTouches[#scrTouches+1] = {
        x = gx,
        y = gy,
        width = 1,
        height = 1,
        id = gid,
    }
end

function love.touchmoved(gid, gx, gy, gp)
    gx = gx * 854
    gy = gy * 480
    for k,v in ipairs(scrTouches) do
        if v.id == gid then
            scrTouches[k] = {
                x = gx,
                y = gy,
                width = 1,
                height = 1,
                id = gid,
            }
        end
    end
end

function love.touchreleased(gid, gx, gy, gp)
    for k,v in ipairs(scrTouches) do
        if v.id == gid then
            table.remove(scrTouches,k)
        end
    end
end

function touchShit()
    -- hacked together
    moveLeft = false
    moveRight = false
    plsJump = false
    plsSwap = false
    for k,v in ipairs(scrTouches) do
        if math.overlap(v, boxLeft) then
            moveLeft = true
        elseif math.overlap(v, boxRight) then
            moveRight = true
        elseif math.overlap(v, boxJump) then
            plsJump = true
        elseif math.overlap(v, boxSwap) then
            plsSwap = true
        end
        if math.overlap(v, boxUp) and not canUp and gameState == "play" then
            canUp = true
            doUp()
        elseif not math.overlap(v, boxUp) then
            canUp = false
        end
    end
end
