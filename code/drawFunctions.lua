function drawPlay(vx, vy)
    map.draw(vx, vy)
    totems.draw(vx, vy)
    player.draw(vx, vy)
end

function drawIntro(vx, vy)
    map.draw(vx, vy)
    if babyZefron ~= nil then
        g.draw(babyZefron.image, babyZefron.x - vx, babyZefron.y - vy, math.rad(79))
    end
    if dadChar ~= nil then
        g.draw(dadChar.image, dadChar.x - vx, dadChar.y - vy)
    end
end

function drawOutro(vx, vy)
    map.draw(vx, vy)
end

function drawIntro2(vx, vy)
    map.draw(vx, vy)
    if babyZefron ~= nil then
        g.draw(babyZefron.image, babyZefron.x - vx, babyZefron.y - vy, math.rad(155))
    end
    if dadChar ~= nil then
        g.draw(dadChar.image, dadChar.x - vx, dadChar.y - vy)
    end
end

function drawMenu(alpha)
    if alpha == nil then alpha = 1 end
    g.setColor(0, 0, 0, 255*alpha)
    g.rectangle("fill", 0, 0, g.getWidth(), g.getHeight())
    g.setColor(255, 255, 255, 255*alpha)
    g.setFont(font.big)
    wil = font.big:getWidth("NEW GAME")
    wio = font.big:getWidth("SKIP INTRO")
    wiq = font.big:getWidth("QUIT GAME")
    hie = font.big:getHeight()
    mOffset = 96
    mults = (480 - mOffset) / 5
    checccc = 0
    checccc = 1
    menuBox[1] = {
        x = 854 / 2 - wio / 2,
        y = (mults * 2) - hie / 4 + mOffset,
        width = wio,
        height = hie,
        nextState = "play-level02",
        title = "SKIP INTRO"
    }
    menuBox[checccc+1] = {
        x = 854 / 2 - wil / 2,
        y = (mults * 3) - hie / 4 + mOffset,
        width = wil,
        height = hie,
        nextState = "intro",
        title = "NEW GAME"
    }
    menuBox[checccc+2] = {
        x = 854 / 2 - wiq / 2,
        y = (mults * 4) - hie / 4 + mOffset,
        width = wiq,
        height = hie,
        nextState = "quit",
        title = "QUIT GAME",
        snd = sfx.exit
    }
    for k,v in ipairs(menuBox) do
        g.print(v.title, v.x, v.y, 0)
    end
    g.setFont(font.hutsut)
    wi = font.hutsut:getWidth("BROKEN DOWN")
    hie = font.big:getHeight()
    bigTitle = {
        x = 854 / 2 - wi / 2,
        y = (mults * 0.5) - hie / 4,
        width = wi,
        height = hie,
        title = "BROKEN DOWN",
    }
    g.print(bigTitle.title, bigTitle.x, bigTitle.y, 0)
end

function drawInstr()
    if gameState == "instr" then
    g.setColor(255, 255, 255)
    g.setFont(font.big)
    ins1 = "Press LEFT and RIGHT to move. Press R"
    ins2 = "to swap player. Press Spacebar or JUMP"
    ins3 = " to jump. Press UP to use warps or portals."
    ins4 = "Click or touch to continue."
    wil = font.big:getWidth(ins1)
    wio = font.big:getWidth(ins2)
    wiq = font.big:getWidth(ins3)
    wir = font.big:getWidth(ins4)
    hie = font.big:getHeight()
    mOffset = 96
    mults = (480 - mOffset) / 5
    menuBox[1] = {
        x = 854 / 2 - wio / 2,
        y = (mults * 1) - hie / 4 + mOffset,
        width = wio,
        height = hie,
        title = ins1
    }
    menuBox[2] = {
        x = 854 / 2 - wil / 2,
        y = (mults * 2) - hie / 4 + mOffset,
        width = wil,
        height = hie,
        title = ins2
    }
    menuBox[3] = {
        x = 854 / 2 - wiq / 2,
        y = (mults * 3) - hie / 4 + mOffset,
        width = wiq,
        height = hie,
        title = ins3,
    }
    menuBox[4] = {
        x = 854 / 2 - wiq / 2,
        y = (mults * 4) - hie / 4 + mOffset,
        width = wiq,
        height = hie,
        title = ins4,
    }
    for k,v in ipairs(menuBox) do
        g.print(v.title, v.x, v.y, 0)
    end
    menuBox = { }
    end
end


function drawPause(vx, vy, alpha)
    map.draw(vx, vy)
    totems.draw(vx, vy)
    player.draw(vx, vy)
    if alpha == nil then alpha = 1 end
    g.setColor(0, 0, 0, 128*alpha)
    g.rectangle("fill", 0, 0, g.getWidth(), g.getHeight())
    g.setColor(255, 255, 255, 255*alpha)
    g.setFont(font.big)
    wio = font.big:getWidth("RESUME GAME")
    wiq = font.big:getWidth("QUIT TO MENU")
    hie = font.big:getHeight()
    mOffset = 96
    menuBox = { }
    mults = (480 - mOffset) / 5
    menuBox[1] = {
        x = 854 / 2 - wio / 2,
        y = (mults * 3) - hie / 4 + mOffset,
        width = wio,
        height = hie,
        nextState = "play",
        title = "RESUME GAME"
    }
    menuBox[2] = {
        x = 854 / 2 - wiq / 2,
        y = (mults * 4) - hie / 4 + mOffset,
        width = wiq,
        height = hie,
        nextState = "menu",
        title = "QUIT TO MENU",
        snd = sfx.pause
    }
    for k,v in ipairs(menuBox) do
        g.print(v.title, v.x, v.y, 0)
    end
end

function drawAndroid()
    if love.system.getOS() == "Android" then
        for k,v in ipairs(scrTouches) do
            g.setColor(255, 255, 255, 128)
            --g.circle("fill", v.x, v.y, 48)
        end
        overlay()
    end
end
