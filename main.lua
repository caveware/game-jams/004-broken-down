--[[
    GGJ Game
  ]]--

function love.load()
    love.audio.setVolume(0)
    gameCanvas = love.graphics.newCanvas(854, 480)
    require "code/start"
    map.load("lvl/intro")
    math.randomseed(os.time())
    scaleW = 854 / 854
    scaleH = 480 / 480
    gameState = "instr"
    nextState = "instr"
    carAnim = 1
    sfx.car:setLooping(true)
    fadeState = 0
    fadeIn = 1
    scrTouches = { }
    subText = ""
    subTimer = 0
    subsWaiting = { }
    moveLeft = false
    moveRight = false
    plsJump = false
    plsSwap = false
    canUp = false
    resumeLevel = nil
    playedSounds = { }
    boxLeft = {
        x = 0,
        y = 400,
        width = 192,
        height = 80
    }
    boxRight = {
        x = 200,
        y = 400,
        width = 192,
        height = 80
    }
    boxJump = {
        x = 662,
        y = 400,
        width = 192,
        height = 80
    }
    boxSwap = {
        x = 662,
        y = 312,
        width = 192,
        height = 80
    }
    boxUp = {
        x = 662,
        y = 224,
        width = 192,
        height = 80
    }
    selectedOption = 1
    menuBox = { }
end

function love.update(dt)
    if dt > 1/15 then dt = 1/15 end
    if fadeIn > 0 and fadeState == 0 then fadeIn = fadeIn - dt * 2 ; if fadeIn < 0 then fadeIn = 0 end end
    subTimer = subTimer - dt
    if subTimer <= 0 then
        if #subsWaiting > 0 then
            subText = subsWaiting[1].text
            subTimer = subsWaiting[1].timer + subTimer
            table.remove(subsWaiting, 1)
            if subsWaiting == nil then subsWaiting = { } end
        else
            subTimer = 0
            subText = ""
        end
    end
    touchShit()
    if babyZefron ~= nil then
        babyZefron.x = babyZefron.x + babyZefron.xSpeed * dt
        babyZefron.y = babyZefron.y + babyZefron.ySpeed * dt
    end
    if gameState == nextState then
        if gameState == "play" then
            player.update(dt)
            view.update()
        elseif gameState == "outro" then
            tri.x = tri.x + dt * 80
            if tri.x > 1036 and alarm == nil then
                tri.x = 1036
                alarm = 0
            end
            if alarm ~= nil then
            alarm = alarm + dt
            if alarm > 3 then
                stopBGM()
                gameState = "menu"
                nextState = "menu"
            end
            end
            view.update()
        elseif gameState == "intro" then
            tmpCX = car.x
            if not math.overlap(car, carEnd) then
                car.x = car.x + 320 * dt
                tmpImage = gfx.car.drive
                if car.x > 1600 then
                    if math.floor(car.x / 150) == math.floor(car.x / 75) / 2 then
                        tmpImage = gfx.car.smoke
                    end
                    if tmpCX <= 1600 then sfx.wrong:play() end
                end
                if car.x > 1800 and tmpCX <=1800 then
                    sfx.dad.noGood:play()
                    addSub("Dad: That can't be good.", 1)
                end
                carAnim = carAnim + dt * 5
                if carAnim > #tmpImage+1 then
                    carAnim = carAnim - #tmpImage
                end
                car.image = tmpImage[math.floor(carAnim)]
            else
                if car.crashed == nil then
                    car.crashed = 0
                    sfx.crash:play()
                    sfx.baby.flyingDad:play()
                    addSub("- Kid: DAAAAAAAAAA...!\n- Dad: BABY ZEFRON, NO!", 3)
                    babyZefron = {
                        x = car.x + 160,
                        y = car.y,
                        xSpeed = 600,
                        ySpeed = -200,
                        image = gfx.baby.still[1]
                    }
                end
                if not sfx.crash:isPlaying() then
                    -- BABY ZEFRON NO
                    if car.crashed == 0 then
                        sfx.dad.babyZefronNo:play()
                        dadChar = {
                            x = car.x + 96,
                            y = car.y + 16,
                            image = gfx.dad.back,
                            width = 64,
                            height = 100,
                            ySpeed = 0
                        }
                        step = 1
                        car.crashed = 1
                    elseif car.crashed == 1 then
                        if babyZefron.x > car.x + 200 then
                            dadChar.x = dadChar.x + 400 * dt
                            step = step + dt * 12
                            if step >= #gfx.dad.walk+1 then
                                step = step - #gfx.dad.walk
                            end
                            dadChar.image = gfx.dad.walk[math.floor(step)]
                        end
                        if dadChar.x > map.getWidth() then
                            nextState = "intro2"
                            fadeState = 0.99
                        end
                    end
                end
                car.image = gfx.car.smoke[3]
                sfx.car:stop()
            end
            fakeCar = {
                x = car.x,
                y = math.min(car.y, car.y + (car.x - 700) * 2),
                width = car.width,
                height = car.height
            }
            view.update()





        elseif gameState == "intro2" then
            if babyZefron.y > 992 then
                dcx = dadChar.x
                dcy = dadChar.y
                dadChar.x = dadChar.x + 400 * dt
                step = step + dt * 12
                if dcx <= 200 and dadChar.x > 200 then
                    sfx.dad.notAgain:play()
                    addSub("Dad: Not again!", 2.5)
                end
                if step >= #gfx.dad.walk+1 then
                    step = step - #gfx.dad.walk
                end
                dadChar.image = gfx.dad.walk[math.floor(step)]
                if dadChar.x > 768 then
                    dadChar.ySpeed = dadChar.ySpeed + 80 * dt
                    if dadChar.ySpeed > 340 then dadChar.ySpeed = 340 end
                    dadChar.image = gfx.dad.still[2]
                    dadChar.y = dadChar.y + dadChar.ySpeed
                end
            end
            if dadChar.y > map.getHeight() then dadChar.x = dcx end
            if dadChar.y > map.getHeight() * 2 and dcy <= map.getHeight() * 2 then
                sfx.ground:play()
                addSub("*THUD!!!*", 0.75)
            end
            if dadChar.y > map.getHeight() * 7 and dcy <= map.getHeight() * 7 then
                sfx.baby.okay:play()
                addSub("Kid: I'm okay, Dad.", 1.5)
            end
            if dadChar.y > map.getHeight()*20 then
                dadChar = nil
                babyZefron = nil
                nextState = "play-level02"
            end
            if dadChar ~= nil then view.update() end






        elseif gameState == "menu" or gameState == "pause" then
                getScale()
            local hScal, vScal = love.window.getWidth() / gameCanvas:getWidth(),
        love.window.getHeight() / gameCanvas:getHeight()

    local x, y = 0, 0

    if hScal > vScal then

        x = (love.window.getWidth() / 2) - (854 * vScal / 2)

    else

        y = (love.window.getHeight() / 2) - (480 * hScal / 2)

    end
                cursor = {
                    x = (love.mouse.getX()-x) / math.min(hScal, vScal),
                    y = (love.mouse.getY()-y) / math.min(hScal, vScal),
                    width = 1,
                    height = 1,
                }
            for k,v in ipairs(menuBox) do
                if love.mouse.isDown('l') and math.overlap(cursor,v) and v.nextState ~= nil then
                    nextState = v.nextState
                    if nextState == "play" then
                        gameState = "play"
                    end
                    if v.snd ~= nil then
                        v.snd:play()
                    end
                end
            end
        end
    else
        fadeState = fadeState + dt * 2
        if fadeState > 1 then
            gameState = nextState
            fadeState = 0
            onStateChange()
        end
    end
    -- Car sound dealing
    if gameState == "intro" and not sfx.car:isPlaying() then
        if car.x < 2000 then stopBGM() ; sfx.car:play() end
    elseif gameState ~= "intro" and sfx.car:isPlaying() then
        sfx.car:stop()
    end
    -- Menu sound dealing
    if gameState == "menu" and not bgm.menu:isPlaying() then
        playBGM(bgm.menu)
    end
    -- Cave sound dealing
    if gameState == "play" and not bgm.cave:isPlaying() then
        playBGM(bgm.cave)
    end

    soundStep()
end

function love.draw()

    love.graphics.setCanvas(gameCanvas)

    love.graphics.setColor(0, 0, 0)

    love.graphics.rectangle('fill', 0, 0, 854, 480)

    g.setColor(255, 255, 255)
    vx = math.round(view.x)
    vy = math.round(view.y)
    if gameState == nextState then
        if gameState == "play" then
            drawPlay(vx, vy)
            drawAndroid()
        elseif gameState == "intro" then
            drawIntro(vx, vy)
        elseif gameState == "intro2" then
            drawIntro2(vx, vy)
        elseif gameState == "menu" then
            drawMenu()
        elseif gameState == "pause" then
            drawPause(vx, vy, 1)
        elseif gameState == "outro" then
            drawOutro(vx, vy)
        elseif gameState == "instr" then
            drawInstr()
        end
    else
        if gameState == "play" or nextState == "play" then
            drawPlay(vx, vy)
            drawAndroid()
        end
        if nextState == "menu" then
            drawMenu(fadeState)
        elseif gameState == "menu" then
            drawMenu(1 - fadeState)
        end
    end
    g.setFont(font.small)
    wid = g.getFont():getWidth(subText)
    if wid == nil then wid = 0 end
    xx = 854 / 2 - wid / 2
    g.setColor(0, 0, 0)
    g.print(subText, xx + 2, 382, 0)
    g.setColor(255, 255, 255)
    g.print(subText, xx, 380, 0)
    if gameState == "outro" and view.x > 500 then
        g.setFont(font.big)
        finalText = "THANKS FOR PLAYING!\n\nLONGER VERSION COMING SOON"
        wid = g.getFont():getWidth(finalText)
        xx = 854 / 2 - wid / 2
        g.printf(finalText, xx, 320, wid, "left", 0)
    end
    if gameState ~= "menu" and gameState ~= "intro" and nextState ~= "menu" then
        exo = fadeState
        if exo == 0 then exo = fadeIn end
        g.setColor(0, 0, 0, 255*exo)
        g.rectangle("fill", 0, 0, g.getWidth(), g.getHeight())
    end

    love.graphics.setCanvas()

    rescale()
end





function getScale()
    scaleW = 1
    scaleH = 1
    return math.max(scaleW, scaleH)
end

function love.keypressed(key)
    if key == "escape" and gameState == nextState then
        if gameState == "menu" then
            sfx.exit:play()
            nextState = "quit"
        elseif gameState == "play" then
            gameState = "pause"
            nextState = "pause"
        elseif gameState == "pause" then
            gameState = "play"
            nextState = "play"
        end
    end
    if key == "up" and gameState == "play" then
        doUp()
    end
    if key == "f" then
        if not love.window.getFullscreen() then
            local _, _, flags = love.window.getMode()
            local width, height = love.window.getDesktopDimensions(flags.display)
            love.window.setMode(width, height)
            love.window.setFullscreen(true)
        else
            love.window.setMode(854, 480)
            love.window.setFullscreen(false)
        end
    end
end

function doUp()
    p = {
            x = player.x,
            y = player.y,
            width = player.width,
            height = player.height
        }
        for k,v in ipairs(arches) do
            dd = true
            if v.exclusive ~= nil then
                if v.exclusive ~= player.state then
                    dd = false
                end
            end
            if math.overlap(p, v) and dd then
                for k,b in ipairs(arches) do

                    if b.name == v.next then
                        sfx.warp:play()
                        player.x = b.x
                        player.y = b.y
                        break
                    end
                end
            end
        end
        if math.overlap(portal, p) and portalCount == 2 then
            nextState = "play-"..portal.level
            if portal.level == "outro" then nextState = "outro" end
            sfx.complete:play()
        end
    totem = totems.baby
    if player.state == "baby" then totem = totems.dad end
    ak = false ; ax = false
    akv = "" ; axv = ""
    for k,v in ipairs(pWarps) do
        if math.overlap(v, player) then
            ak = true
            akv = v
        elseif math.overlap(v, totem) then
            ax = true
            axv = v
        end
    end
    if (ak and ax) and (akv.name == axv.link) and (axv.name == akv.link) then
        player.transmod = -1
        player.transition = 1
        wid = player.width - totem.width
        hei = player.height - totem.height
        player.x = totem.x - wid / 2
        player.y = totem.y - hei
        tx = totem.x ; ty = totem.y
        totem.x = p.x + wid / 2
        totem.y = p.y + hei
        if map.touches(totem) or map.touches(player) then
            player.transmod = 1
            player.transition = 0
            player.x = p.x
            player.y = p.y
            totem.x = tx
            totem.y = ty
        else
            sfx.pipe:play()
        end
    end
end

function onStateChange()
    if gameState == "quit" then
        love.event.quit()
    elseif gameState == "menu" then
        subsWaiting = { }
        playBGM(bgm.menu)
    elseif gameState == "intro" then
        subsWaiting = { }
        playedSounds = { }
        map.load("lvl/intro")
        sfx.dad.engine:play()
        addSub("Dad: And that's how your mo... is something wrong with the engine?", 3.5)
    elseif gameState == "intro2" then
        subsWaiting = { }
        map.load("lvl/level01")
            babyZefron.y = -32
            babyZefron.x = -32
            babyZefron.ySpeed = 800
            dadChar.y = 948
            dadChar.x = -128
    elseif gameState:find("play-") == 1 then
        map.load("lvl/" .. gameState:sub(6))
        resumeLevel = gameState
        fadeIn = 1
        gameState = "play"
        nextState = "play"
    elseif gameState == "outro" then
        map.load("lvl/outro")
        sfx.baby.wellGosh:play()
        addSub("Kid: Well... gosh.", 3)
        playBGM(bgm.aggressive)
        bgm.aggressive:setVolume(0.5)
        tri = {x = 200, y = 600, width = 40, height = 40}
    end
end

function addSub(txt, tmer)
    subsWaiting[#subsWaiting+1] = {
        text = txt,
        timer = tmer
    }
end

function love.mousepressed(x, y, b)
    if nextState == "instr" then
        gameState = "menu"
        nextState = "menu"
    end
end


function rescale()

    local hScal, vScal = love.window.getWidth() / gameCanvas:getWidth(),
        love.window.getHeight() / gameCanvas:getHeight()

    local x, y = 0, 0

    if hScal > vScal then

        x = (love.window.getWidth() / 2) - (854 * vScal / 2)

    else

        y = (love.window.getHeight() / 2) - (480 * hScal / 2)

    end

    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(gameCanvas, x, y, 0, math.min(hScal, vScal))

end
