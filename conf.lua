-- Configure the game start
function love.conf(t)
    t.window.width = 854
    t.window.minwidth = 854
    t.window.height = 480
    t.window.minheight = 480
    t.window.fsaa = 2
    t.window.icon = "gfx/window/icon.png"
    t.window.title = "Broken Down"
    t.modules.joystick = false
    t.modules.physics = false
    t.window.resizable = true
    t.window.vsync = false
end
